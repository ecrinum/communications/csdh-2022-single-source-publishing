<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href="styles.css" />
	<title>The Importance of Single Source Publishing in Scientific
Publishing</title>
</head>
<body>
	<header>
		<h1>The Importance of Single Source Publishing in Scientific
Publishing</h1>
		<p>Antoine Fauchié, Margot Mellet, Marcello Vitali-Rosati. Canada
Research Chair on Digital Textualites
(CC-BY). Source: <a href="https://ssp.digitaltextualities.ca">ssp.digitaltextualities.ca</a></p>
	</header>
	<div class="wrapper">
		<section id="what-is-single-source-publishing" class="un">
  <h2>What is Single Source Publishing?</h2>
  <p><strong>The expression <em>Single Source Publishing</em> refers to
  generating several formats from a single source.</strong> One and
  unique source can be used to produce various artifacts, without having
  to switch from one working progress to another. With a Single Source
  Publishing approach it’s possible to produce a PDF format for
  printing, an XML export for a digital platform or a digital version in
  HTML format, just with <em>one</em> source. What is this “source”? A
  set of texts, some metadata, some bibliographical data, perhaps some
  images and other medias. Perhaps you already use the principles of
  Single Source Publishing: it’s long and tedious with a classic word
  processor, it’s a little bit complex with LaTeX, it’s expensive and
  it’s not easy with XML, and it’s very powerful and more accessible
  with tools like Pandoc.</p>
  <p><strong>This editorial challenge brings up both theoretical and
  technical questions, such as the legitimization of content, the
  evolution of publishing practices, and the creation of adequate
  tools.</strong></p>
  </section>
  <div class="deux">
  <p><img src="single-source-publishing.png" /></p>
  </div>
  <section id="academic-publishing-constraints" class="trois">
  <h2>Academic publishing constraints</h2>
  <p>Scientific publishing has several constraints:</p>
  <ul>
  <li><strong>critical material</strong>: footnotes, citations,
  bibliographies, figures, insert, etc.</li>
  <li><strong>metadata</strong>: data about data, like title, subtitle,
  authors, standard identifiers (ORCID, Wikidata, etc.)</li>
  <li><strong>text structuration</strong>: semantic is a necessity for
  digital diffusion (especially in XML)</li>
  <li><strong>bibliographic data</strong>: structured references</li>
  <li><strong>peer review</strong>: a complex circulation and validation
  for the texts</li>
  <li><strong>specific formats</strong>: articles, books, conference
  proceedings, etc.</li>
  <li><strong>publish or perish</strong>: publish a lot but slowly</li>
  </ul>
  </section>
  <section id="different-formats-in-and-out" class="quatre">
  <h2>Different formats: in and out</h2>
  <p><strong>In</strong>: with non-standard formats like
  <code>.doc</code>/<code>.docx</code>, it’s not possible to produce
  richly structured content. XML is a good solution but it’s complex and
  there are not enough customizable tools to write and edit. Lightweight
  and understandable markup languages is an interesting intermediate
  solution, like Markdown or AsciiDoc.</p>
  <p><strong>Out</strong>: scientific publishing requires specific
  formats: PDF, HTML, and various XML with specific schemas (like JATS
  or TEI).</p>
  </section>
  <section id="benefits-of-single-source-publishing" class="cinq">
  <h2>Benefits of Single Source Publishing</h2>
  <ul>
  <li><strong>one source</strong>: no need to manage multiple versions,
  like the <code>.doc</code> version, the <code>.xml</code> version and
  the <code>.indd</code> version</li>
  <li><strong>horizontal workflow</strong>: all actors of the publishing
  chain can participate at the same time (in theory). Writers,
  publishers, designers and digital distributors can work at the same
  time</li>
  <li><strong>multimodal</strong>: one source but multiple
  artifacts/formats</li>
  <li><strong>time and energy saving</strong></li>
  <li><strong>work in progress</strong>: building a publishing chain can
  be a work in progress</li>
  </ul>
  </section>
  <section
  id="issues-involved-in-the-single-source-publishing-principle"
  class="six">
  <h2>Issues involved in the Single Source Publishing principle</h2>
  <h3 id="legitimization-of-content">Legitimization of content</h3>
  <p>If the publishing chain can be horizontal, how can we do the
  legitimization of the content? How to deal with the validation of
  content, the structuration and the design of the document?</p>
  <h3 id="evolution-of-publishing-practices">Evolution of publishing
  practices</h3>
  <ul>
  <li>no more word processing (or much more less)</li>
  <li>more tech in publishing staff (but less complexity)</li>
  <li>publishers build their own publishing chain by assembling free
  programs/software</li>
  <li>publishing workflows can be fun!</li>
  </ul>
  <h3 id="creation-of-adequate-tools">Creation of adequate tools</h3>
  <p>Since the 1980’s, scientific community use one and unique tool for
  writing and editing: different generations of word processors like
  Microsoft Word, LibreOffice Writer or Google Docs. Proprietary,
  copycat of proprietary software or centralised, they maintain a
  confusion between the structure of the contents and the graphic
  rendering.</p>
  <p><strong>The academic community needs tools that correspond to its
  constraints!</strong> And it’s what a lot of people do, with an
  approach more or less compatible with the Single Source Publishing:
  Manifold, PubPubPub, Coko, Métopes, Quire, Stylo, etc.</p>
  </section>
  <section id="concepts" class="sept">
  <h2>Concepts</h2>
  <p>With Single Source Publishing come some interesting concepts:</p>
  <h3 id="hybridity">Hybridity</h3>
  <p>From Marshall McLuhan <span class="citation"
  data-cites="mcluhan_understanding_1965">(McLuhan, 1965)</span>: the
  hybridity of the media generates new media: the media have an effect
  on each other. In the same way, it is interesting to take into account
  the influence of various formats on their unique source.</p>
  <h3 id="hybridization">Hybridization</h3>
  <p>From Alessandro Ludovico <span class="citation"
  data-cites="ludovico_post-digital_2012">(Ludovico &amp; Cramer,
  2012)</span>: the hybridization of forms and formats is a phenomenon
  that can be observed in the first digital experiments: electronic
  versions come to complete already existing forms, printed and digital
  artifacts become hybrid.</p>
  <h3 id="editorialization">Editorialization</h3>
  <p>From Marcello Vitali-Rosati <span class="citation"
  data-cites="vitali-rosati_what_2016">(Vitali-Rosati, 2016)</span>:
  editorialization is the set of dynamics that produce and structure
  digital space. The adaptation of publishing chains to produce
  different artifacts is one way of understanding and constructing this
  space.</p>
  </section>
  <section id="academic-single-source-publishing-in-practice-an-example"
  class="huit">
  <h2>Academic Single Source Publishing in practice: an example</h2>
  <p>In the <strong>Revue2.0 project</strong> (<a
  href="https://revue20.org">revue20.org</a>) lead by the Canada
  Research Chair on Digital Textualities, several journals have
  experimented Single Source Publishing with Stylo (a semantic editor
  for academic writing and publishing). With one source (a set of text,
  metadata, bibliographic data), scientific journals are able to produce
  the following formats: HTML for their website, PDF for their own
  distribution and for aggregators, and XML formats for digital
  distributors.</p>
  <p><strong>Stylo</strong> is a tool designed to transform the digital
  workflow of scholarly journals in humanities and social sciences. As a
  WYSIWYM (What You See Is What You Mean) semantic text editor for the
  humanities, it aims to improve the academic publishing chain. Stylo is
  ready and free to use: <a
  href="https://stylo.huma-num.fr">stylo.huma-num.fr</a>.</p>
  </section>
  <section id="references" class="neuf">
  <h2>References</h2>
  <div id="refs" class="references csl-bib-body hanging-indent"
  data-line-spacing="2" role="doc-bibliography">
  <div id="ref-blanc_technologies_2018" class="csl-entry"
  role="doc-biblioentry">
  Blanc, J., &amp; Haute, L. (2018). <span>Technologies de l’édition
  numérique</span>. <em>Sciences du design</em>, <em>8</em>(2), 11–17.
  </div>
  <div id="ref-hyde_single_2021" class="csl-entry"
  role="doc-biblioentry">
  Hyde, A. (2021). Single <span>Source Publishing</span>. In
  <em>Coko</em>.
  </div>
  <div id="ref-ludovico_post-digital_2012" class="csl-entry"
  role="doc-biblioentry">
  Ludovico, A., &amp; Cramer, F. (2012). <em><span>Post-digital print:
  the mutation of publishing since 1894</span></em>.
  <span>Onomatopee</span>.
  </div>
  <div id="ref-maxwell_mind_2019" class="csl-entry"
  role="doc-biblioentry">
  Maxwell, J. W. (2019). <em>Mind the <span>Gap</span>: <span>A
  Landscape Analysis</span> of <span>Open Source Publishing Tools</span>
  and <span>Platforms</span></em>. <span>The MIT Press</span>.
  </div>
  <div id="ref-mcluhan_understanding_1965" class="csl-entry"
  role="doc-biblioentry">
  McLuhan, M. (1965). <em><span>Understanding media: the extensions of
  man.</span></em> <span>McGraw-Hill</span>.
  </div>
  <div id="ref-vitali-rosati_what_2016" class="csl-entry"
  role="doc-biblioentry">
  Vitali-Rosati, M. (2016). What is editorialization ? <em>Sens
  Public</em>.
  </div>
  </div>
  </section>
	</div>
	<footer>
		This poster was made with Vim, Markdown, YAML, Pandoc and Git. It
  exists in different versions at this address:
  ssp.digitaltextualities.ca
	</footer>
</body>
</html>
